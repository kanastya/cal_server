package com.fakeeyebrowsinc.tocal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TocalApplication {

	public static void main(String[] args) {
		SpringApplication.run(TocalApplication.class, args);
	}

}
