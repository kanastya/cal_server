package com.fakeeyebrowsinc.tocal;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.fakeeyebrowsinc.calculator.implementation.Calculator;
import com.fakeeyebrowsinc.calculator.implementation.InvalidOperatorException;


@RestController

public class Controller {

	@RequestMapping("/test")
	public DataCal greeting() {
		DataCal datacal = new DataCal();
		datacal.setExpression("2 + 13");
		Calculator calculator = new Calculator();
		try {
			double res = calculator.compute(datacal.getExpression());
			datacal.setResult(res+"");
		} catch (IllegalArgumentException | InvalidOperatorException e) {
			datacal.setResult("error");
			datacal.setErrors(e.getMessage());
		}
		return datacal;
	}
	
	
	@GetMapping("/")
	public ModelAndView home() {
		return new ModelAndView("index");
	}
	
	@PostMapping("/api/calc")
	public DataCal calc(@RequestBody DataCal datacal) {
		Calculator calculator = new Calculator();
		try {
			double res = calculator.compute(datacal.getExpression());
			datacal.setResult(res+"");
		} catch (IllegalArgumentException | InvalidOperatorException e) {
			datacal.setResult("error");
			datacal.setErrors(e.getMessage());
		}
		return datacal;
	}


}

