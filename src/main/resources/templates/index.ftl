<!DOCTYPE html>
<html>
<head>
	<title>Calculator</title>
  	<meta charset="UTF-8">
  	<meta content="width=device-width, initial-scale=1, shrink-to-fit=no"  name="viewport">
  	
</head>
 
<body>  
<script>
 function postJSONNoSpinner(api,bodyDTO, parser) {
        fetch(api , {
            credentials: 'include',
            method: 'POST',          
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'              
            },           
            body: JSON.stringify(bodyDTO)
          })      
          .then(response => {            
            if (response.ok) { 
                return response.json();              
            }else{
            
                window.alert(api +" " + response.status + " " + response.statusText,3)
            }
          }) 
          .then(res=> {
              if(typeof res != "undefined"){
                parser(api,res)
              }
          })
          .catch(res=> {
             
              window.alert(api +" " + res,3,3)
            }
           ) 
    }
    
function myFunction() {
 data={
  expression: document.getElementById('expression').value,
  result : "",
  error: ""
 }
 postJSONNoSpinner("api/calc",data,(query,result)=>{
 document.getElementById('result').innerHTML = result.result;
 document.getElementById('error').innerHTML = result.errors;
 });
}
</script>
   <div>
   		<p><span style="color:blue">Example: 2 + 2</br>Root: 2 Alt+251 16 = 4</span></p>
   		<input id="expression" type="text">
  		<button onclick="myFunction()">=</button>
  		<output id="result"></output>
   </div>
   <div>
  	<p><span id="error" style="color:red"></span></p>
   </div>
   
  </body>
</html>